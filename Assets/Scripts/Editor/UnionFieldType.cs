#if UNITY_EDITOR
namespace FlatBuffers
{
    internal struct UnionFieldType
    {
        public byte Value { get; set; }
    }
}
#endif