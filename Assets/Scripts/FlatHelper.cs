﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System.IO;

namespace FlatBuffers
{
    public static class FlatHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="path"></param>
        public static void Save(FlatBufferBuilder builder, string path)
        {
            File.WriteAllBytes(path, builder.SizedByteArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static ByteBuffer Load(string path)
        {
            return new ByteBuffer(File.ReadAllBytes(path));
        }
    }
}