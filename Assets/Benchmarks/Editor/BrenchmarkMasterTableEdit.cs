/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

#if UNITY_EDITOR
namespace FlatBufferGenerated.BrenchmarkFlatBuffer
{
    public class BrenchmarkMasterTableEdit
    {
        internal static T CastTo<T>(object o) => (T) o;

        internal static T CasTo<T>(T inferNeededType, object o) where T : class
        {
            return o as T;
        }

        public static object[,] Execute(object data, FlatBuffers.ItemInfoCollapse itemInfoCollapse)
        {
            var dataTable = (BrenchmarkMasterTable) data;
            object[,] results = null;
            if (itemInfoCollapse.Name == "ItemA")
            {
                results = new object[dataTable.DataLength, itemInfoCollapse.NameTypeField.Length];
                for (int i = 0; i < dataTable.DataLength; i++)
                {
                    var idata = dataTable.Data(i);
                    if (idata != null)
                    {
                        results[i, 0] = idata.Value.Id;
                        results[i, 1] = idata.Value.Name;
                        results[i, 2] = idata.Value.Hp;
                    }
                }
            }

            if (itemInfoCollapse.Name == "ItemB")
            {
                results = new object[dataTable.TestArrayLength, itemInfoCollapse.NameTypeField.Length];
                for (int i = 0; i < dataTable.TestArrayLength; i++)
                {
                    var idata = dataTable.TestArray(i);
                    if (idata != null)
                    {
                        results[i, 0] = idata.Value.Id;
                        results[i, 1] = idata.Value.Name;
                        var __missionnames = new object[idata.Value.MissionNamesLength];
                        for (int j = 0; j < idata.Value.MissionNamesLength; j++)
                        {
                            __missionnames[j] = idata.Value.MissionNames(j);
                        }

                        results[i, 2] = __missionnames;
                        var __missionvalues = new object[idata.Value.MissionValuesLength];
                        for (int j = 0; j < idata.Value.MissionValuesLength; j++)
                        {
                            __missionvalues[j] = idata.Value.MissionValues(j);
                        }

                        results[i, 3] = __missionvalues;
                    }
                }
            }

            return results;
        }
    }
}

#endif