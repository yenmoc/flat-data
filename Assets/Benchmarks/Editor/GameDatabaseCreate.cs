/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

// This class is auto generated

#if UNITY_EDITOR
using FlatBuffers;
using UnityEditor;
using UnityEngine;

namespace FlatBufferGenerated.BrenchmarkFlatBuffer
{
    public static class GameDatabaseCreate
    {
        public static void Run(string path, string name)
        {
            var builder = new FlatBufferBuilder(1);

var itema_sortedvector =  ItemA.CreateSortedVectorOfItemA(builder, new[] {ItemA.CreateItemA(builder, builder.CreateString("Id_1"),builder.CreateString("Name Item 1"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_2"),builder.CreateString("Name Item 2"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_3"),builder.CreateString("Name Item 3"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_4"),builder.CreateString("Name Item 4"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_5"),builder.CreateString("Name Item 5"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_6"),builder.CreateString("Name Item 6"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_7"),builder.CreateString("Name Item 7"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_8"),builder.CreateString("Name Item 8"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_9"),builder.CreateString("Name Item 9"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_10"),builder.CreateString("Name Item 10"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_11"),builder.CreateString("Name Item 11"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_12"),builder.CreateString("Name Item 12"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_13"),builder.CreateString("Name Item 13"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_14"),builder.CreateString("Name Item 14"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_15"),builder.CreateString("Name Item 15"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_16"),builder.CreateString("Name Item 16"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_17"),builder.CreateString("Name Item 17"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_18"),builder.CreateString("Name Item 18"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_19"),builder.CreateString("Name Item 19"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_20"),builder.CreateString("Name Item 20"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_21"),builder.CreateString("Name Item 21"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_22"),builder.CreateString("Name Item 22"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_23"),builder.CreateString("Name Item 23"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_24"),builder.CreateString("Name Item 24"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_25"),builder.CreateString("Name Item 25"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_26"),builder.CreateString("Name Item 26"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_27"),builder.CreateString("Name Item 27"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_28"),builder.CreateString("Name Item 28"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_29"),builder.CreateString("Name Item 29"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_30"),builder.CreateString("Name Item 30"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_31"),builder.CreateString("Name Item 31"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_32"),builder.CreateString("Name Item 32"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_33"),builder.CreateString("Name Item 33"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_34"),builder.CreateString("Name Item 34"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_35"),builder.CreateString("Name Item 35"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_36"),builder.CreateString("Name Item 36"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_37"),builder.CreateString("Name Item 37"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_38"),builder.CreateString("Name Item 38"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_39"),builder.CreateString("Name Item 39"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_40"),builder.CreateString("Name Item 40"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_41"),builder.CreateString("Name Item 41"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_42"),builder.CreateString("Name Item 42"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_43"),builder.CreateString("Name Item 43"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_44"),builder.CreateString("Name Item 44"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_45"),builder.CreateString("Name Item 45"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_46"),builder.CreateString("Name Item 46"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_47"),builder.CreateString("Name Item 47"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_48"),builder.CreateString("Name Item 48"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_49"),builder.CreateString("Name Item 49"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_50"),builder.CreateString("Name Item 50"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_51"),builder.CreateString("Name Item 51"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_52"),builder.CreateString("Name Item 52"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_53"),builder.CreateString("Name Item 53"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_54"),builder.CreateString("Name Item 54"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_55"),builder.CreateString("Name Item 55"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_56"),builder.CreateString("Name Item 56"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_57"),builder.CreateString("Name Item 57"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_58"),builder.CreateString("Name Item 58"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_59"),builder.CreateString("Name Item 59"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_60"),builder.CreateString("Name Item 60"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_61"),builder.CreateString("Name Item 61"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_62"),builder.CreateString("Name Item 62"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_63"),builder.CreateString("Name Item 63"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_64"),builder.CreateString("Name Item 64"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_65"),builder.CreateString("Name Item 65"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_66"),builder.CreateString("Name Item 66"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_67"),builder.CreateString("Name Item 67"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_68"),builder.CreateString("Name Item 68"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_69"),builder.CreateString("Name Item 69"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_70"),builder.CreateString("Name Item 70"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_71"),builder.CreateString("Name Item 71"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_72"),builder.CreateString("Name Item 72"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_73"),builder.CreateString("Name Item 73"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_74"),builder.CreateString("Name Item 74"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_75"),builder.CreateString("Name Item 75"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_76"),builder.CreateString("Name Item 76"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_77"),builder.CreateString("Name Item 77"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_78"),builder.CreateString("Name Item 78"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_79"),builder.CreateString("Name Item 79"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_80"),builder.CreateString("Name Item 80"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_81"),builder.CreateString("Name Item 81"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_82"),builder.CreateString("Name Item 82"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_83"),builder.CreateString("Name Item 83"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_84"),builder.CreateString("Name Item 84"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_85"),builder.CreateString("Name Item 85"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_86"),builder.CreateString("Name Item 86"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_87"),builder.CreateString("Name Item 87"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_88"),builder.CreateString("Name Item 88"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_89"),builder.CreateString("Name Item 89"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_90"),builder.CreateString("Name Item 90"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_91"),builder.CreateString("Name Item 91"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_92"),builder.CreateString("Name Item 92"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_93"),builder.CreateString("Name Item 93"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_94"),builder.CreateString("Name Item 94"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_95"),builder.CreateString("Name Item 95"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_96"),builder.CreateString("Name Item 96"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_97"),builder.CreateString("Name Item 97"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_98"),builder.CreateString("Name Item 98"),100),
ItemA.CreateItemA(builder, builder.CreateString("Id_99"),builder.CreateString("Name Item 99"),100)});
var itemb_sortedvector =  ItemB.CreateSortedVectorOfItemB(builder, new[] {ItemB.CreateItemB(builder, 1,builder.CreateString("Level 1"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Dish"),builder.CreateString("Customer")}),ItemB.CreateMissionValuesVector(builder, new []{10,10})),
ItemB.CreateItemB(builder, 2,builder.CreateString("Level 2"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Coin"),builder.CreateString("Customer")}),ItemB.CreateMissionValuesVector(builder, new []{140,10})),
ItemB.CreateItemB(builder, 3,builder.CreateString("Level 3"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Coin"),builder.CreateString("Customer")}),ItemB.CreateMissionValuesVector(builder, new []{225,12})),
ItemB.CreateItemB(builder, 4,builder.CreateString("Level 4"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Coin"),builder.CreateString("Customer")}),ItemB.CreateMissionValuesVector(builder, new []{190,13})),
ItemB.CreateItemB(builder, 5,builder.CreateString("Level 5"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Coin"),builder.CreateString("Customer")}),ItemB.CreateMissionValuesVector(builder, new []{285,13})),
ItemB.CreateItemB(builder, 6,builder.CreateString("Level 6"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Coin"),builder.CreateString("LeavingCustomer")}),ItemB.CreateMissionValuesVector(builder, new []{395,1})),
ItemB.CreateItemB(builder, 7,builder.CreateString("Level 7"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Like"),builder.CreateString("Customer"),builder.CreateString("Burn")}),ItemB.CreateMissionValuesVector(builder, new []{13,14,1})),
ItemB.CreateItemB(builder, 8,builder.CreateString("Level 8"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Dish"),builder.CreateString("Customer"),builder.CreateString("Trash")}),ItemB.CreateMissionValuesVector(builder, new []{43,17,1})),
ItemB.CreateItemB(builder, 9,builder.CreateString("Level 9"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Like"),builder.CreateString("Customer"),builder.CreateString("Trash")}),ItemB.CreateMissionValuesVector(builder, new []{13,15,1})),
ItemB.CreateItemB(builder, 10,builder.CreateString("Level 10"),ItemB.CreateMissionNamesVector(builder, new []{builder.CreateString("Coin"),builder.CreateString("Customer"),builder.CreateString("Trash")}),ItemB.CreateMissionValuesVector(builder, new []{605,15,1}))});
var root = BrenchmarkMasterTable.CreateBrenchmarkMasterTable(builder, itema_sortedvector,itemb_sortedvector);
builder.Finish(root.Value);

            FlatHelper.Save(builder, path + $"/{name}.wr");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log("<color=#25854B>Generate binary file complete!</color>");
        }
    }
}
#endif
