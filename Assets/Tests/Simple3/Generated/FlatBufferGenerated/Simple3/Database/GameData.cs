// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace FlatBufferGenerated.Simple3.Database
{

using global::System;
using global::FlatBuffers;

public struct GameData : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static GameData GetRootAsGameData(ByteBuffer _bb) { return GetRootAsGameData(_bb, new GameData()); }
  public static GameData GetRootAsGameData(ByteBuffer _bb, GameData obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public GameData __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public Vec3? Position { get { int o = __p.__offset(4); return o != 0 ? (Vec3?)(new Vec3()).__assign(o + __p.bb_pos, __p.bb) : null; } }
  public int Mana { get { int o = __p.__offset(6); return o != 0 ? __p.bb.GetInt(o + __p.bb_pos) : (int)0; } }
  public bool MutateMana(int Mana) { int o = __p.__offset(6); if (o != 0) { __p.bb.PutInt(o + __p.bb_pos, Mana); return true; } else { return false; } }
  public int Hp { get { int o = __p.__offset(8); return o != 0 ? __p.bb.GetInt(o + __p.bb_pos) : (int)0; } }
  public bool MutateHp(int Hp) { int o = __p.__offset(8); if (o != 0) { __p.bb.PutInt(o + __p.bb_pos, Hp); return true; } else { return false; } }
  public string Name { get { int o = __p.__offset(10); return o != 0 ? __p.__string(o + __p.bb_pos) : null; } }
#if ENABLE_SPAN_T
  public Span<byte> GetNameBytes() { return __p.__vector_as_span(10); }
#else
  public ArraySegment<byte>? GetNameBytes() { return __p.__vector_as_arraysegment(10); }
#endif
  public byte[] GetNameArray() { return __p.__vector_as_array<byte>(10); }
  public EColor Color { get { int o = __p.__offset(12); return o != 0 ? (EColor)__p.bb.GetInt(o + __p.bb_pos) : EColor.Red; } }
  public bool MutateColor(EColor Color) { int o = __p.__offset(12); if (o != 0) { __p.bb.PutInt(o + __p.bb_pos, (int)Color); return true; } else { return false; } }
  public WeaponUnion WeaponType { get { int o = __p.__offset(14); return o != 0 ? (WeaponUnion)__p.bb.Get(o + __p.bb_pos) : WeaponUnion.NONE; } }
  public bool MutateWeaponType(WeaponUnion Weapon_type) { int o = __p.__offset(14); if (o != 0) { __p.bb.Put(o + __p.bb_pos, (byte)Weapon_type); return true; } else { return false; } }
  public TTable? Weapon<TTable>() where TTable : struct, IFlatbufferObject { int o = __p.__offset(16); return o != 0 ? (TTable?)__p.__union<TTable>(o) : null; }
  public byte Inventory(int j) { int o = __p.__offset(18); return o != 0 ? __p.bb.Get(__p.__vector(o) + j * 1) : (byte)0; }
  public int InventoryLength { get { int o = __p.__offset(18); return o != 0 ? __p.__vector_len(o) : 0; } }
#if ENABLE_SPAN_T
  public Span<byte> GetInventoryBytes() { return __p.__vector_as_span(18); }
#else
  public ArraySegment<byte>? GetInventoryBytes() { return __p.__vector_as_arraysegment(18); }
#endif
  public byte[] GetInventoryArray() { return __p.__vector_as_array<byte>(18); }
  public bool MutateInventory(int j, byte inventory) { int o = __p.__offset(18); if (o != 0) { __p.bb.Put(__p.__vector(o) + j * 1, inventory); return true; } else { return false; } }

  public static void StartGameData(FlatBufferBuilder builder) { builder.StartObject(8); }
  public static void AddPosition(FlatBufferBuilder builder, Offset<Vec3> PositionOffset) { builder.AddStruct(0, PositionOffset.Value, 0); }
  public static void AddMana(FlatBufferBuilder builder, int Mana) { builder.AddInt(1, Mana, 0); }
  public static void AddHp(FlatBufferBuilder builder, int Hp) { builder.AddInt(2, Hp, 0); }
  public static void AddName(FlatBufferBuilder builder, StringOffset NameOffset) { builder.AddOffset(3, NameOffset.Value, 0); }
  public static void AddColor(FlatBufferBuilder builder, EColor Color) { builder.AddInt(4, (int)Color, 0); }
  public static void AddWeaponType(FlatBufferBuilder builder, WeaponUnion WeaponType) { builder.AddByte(5, (byte)WeaponType, 0); }
  public static void AddWeapon(FlatBufferBuilder builder, int WeaponOffset) { builder.AddOffset(6, WeaponOffset, 0); }
  public static void AddInventory(FlatBufferBuilder builder, VectorOffset inventoryOffset) { builder.AddOffset(7, inventoryOffset.Value, 0); }
  public static VectorOffset CreateInventoryVector(FlatBufferBuilder builder, byte[] data) { builder.StartVector(1, data.Length, 1); for (int i = data.Length - 1; i >= 0; i--) builder.AddByte(data[i]); return builder.EndVector(); }
  public static VectorOffset CreateInventoryVectorBlock(FlatBufferBuilder builder, byte[] data) { builder.StartVector(1, data.Length, 1); builder.Add(data); return builder.EndVector(); }
  public static void StartInventoryVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(1, numElems, 1); }
  public static Offset<GameData> EndGameData(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<GameData>(o);
  }
};


}
