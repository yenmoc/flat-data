﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

namespace Simple1.Database
{
    public class Reward
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}