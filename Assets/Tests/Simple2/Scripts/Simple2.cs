﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

namespace Simple2.Database
{
    public enum EnemyType : byte
    {
        Warrios,
        Magician,
        Pirate,
        Thief,
        Beginner,
    }

    public class Enemy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public EnemyType EnemyType { get; set; }
    }

    public class EnemyContainer
    {
        public Enemy[] enemies;
    }
}