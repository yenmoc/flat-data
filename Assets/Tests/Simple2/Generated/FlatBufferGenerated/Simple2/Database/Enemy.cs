// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace FlatBufferGenerated.Simple2.Database
{

using global::System;
using global::FlatBuffers;

public struct Enemy : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static Enemy GetRootAsEnemy(ByteBuffer _bb) { return GetRootAsEnemy(_bb, new Enemy()); }
  public static Enemy GetRootAsEnemy(ByteBuffer _bb, Enemy obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public Enemy __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public int Id { get { int o = __p.__offset(4); return o != 0 ? __p.bb.GetInt(o + __p.bb_pos) : (int)0; } }
  public bool MutateId(int Id) { int o = __p.__offset(4); if (o != 0) { __p.bb.PutInt(o + __p.bb_pos, Id); return true; } else { return false; } }
  public string Name { get { int o = __p.__offset(6); return o != 0 ? __p.__string(o + __p.bb_pos) : null; } }
#if ENABLE_SPAN_T
  public Span<byte> GetNameBytes() { return __p.__vector_as_span(6); }
#else
  public ArraySegment<byte>? GetNameBytes() { return __p.__vector_as_arraysegment(6); }
#endif
  public byte[] GetNameArray() { return __p.__vector_as_array<byte>(6); }
  public EnemyType EnemyType { get { int o = __p.__offset(8); return o != 0 ? (EnemyType)__p.bb.Get(o + __p.bb_pos) : EnemyType.Warrios; } }
  public bool MutateEnemyType(EnemyType EnemyType) { int o = __p.__offset(8); if (o != 0) { __p.bb.Put(o + __p.bb_pos, (byte)EnemyType); return true; } else { return false; } }

  public static Offset<Enemy> CreateEnemy(FlatBufferBuilder builder,
      int Id = 0,
      StringOffset NameOffset = default(StringOffset),
      EnemyType EnemyType = EnemyType.Warrios) {
    builder.StartObject(3);
    Enemy.AddName(builder, NameOffset);
    Enemy.AddId(builder, Id);
    Enemy.AddEnemyType(builder, EnemyType);
    return Enemy.EndEnemy(builder);
  }

  public static void StartEnemy(FlatBufferBuilder builder) { builder.StartObject(3); }
  public static void AddId(FlatBufferBuilder builder, int Id) { builder.AddInt(0, Id, 0); }
  public static void AddName(FlatBufferBuilder builder, StringOffset NameOffset) { builder.AddOffset(1, NameOffset.Value, 0); }
  public static void AddEnemyType(FlatBufferBuilder builder, EnemyType EnemyType) { builder.AddByte(2, (byte)EnemyType, 0); }
  public static Offset<Enemy> EndEnemy(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<Enemy>(o);
  }
};


}
