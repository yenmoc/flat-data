# Changelog
All notable changes to this project will be documented in this file.

## [0.0.7] - 02-02-2020
### Changed
	-update code generate from spreadsheet

## [0.0.6] - 01-02-2020
### Changed
	-check empty name when generate binarty file
	-check correct path

## [0.0.3] - 01-02-2020
### Changed
	- find correct property name when case delete raw code file -> using property of class generated
